package com.example.examenrecuperacion_adriancastaeda;

import androidx.appcompat.app.AppCompatActivity;

public class CuentaBanco extends AppCompatActivity {
    private String numCuenta;
    private String nombre;
    private String banco;
    private float saldo;

    public CuentaBanco(String numCuenta, String nombre, String banco, float saldo) {
        this.numCuenta = numCuenta;
        this.nombre = nombre;
        this.banco = banco;
        this.saldo = saldo;
    }

    public String getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public float obtenerSaldo() {
        return saldo;
    }

    public void retirarDinero(float cantidad) {
        if (cantidad <= saldo) {
            saldo -= cantidad;
        } else {
            throw new IllegalArgumentException("Saldo insuficiente");
        }
    }

    public void hacerDeposito(float cantidad) {
        if (cantidad > 0) {
            saldo += cantidad;
        } else {
            throw new IllegalArgumentException("La cantidad a depositar debe ser positiva");
        }
    }
}

